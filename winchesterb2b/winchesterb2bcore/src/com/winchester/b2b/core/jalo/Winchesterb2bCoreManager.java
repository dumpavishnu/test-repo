/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.winchester.b2b.core.jalo;

import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.extension.ExtensionManager;
import com.winchester.b2b.core.constants.Winchesterb2bCoreConstants;
import com.winchester.b2b.core.setup.CoreSystemSetup;


/**
 * Do not use, please use {@link CoreSystemSetup} instead.
 * 
 */
public class Winchesterb2bCoreManager extends GeneratedWinchesterb2bCoreManager
{
	public static final Winchesterb2bCoreManager getInstance()
	{
		final ExtensionManager em = JaloSession.getCurrentSession().getExtensionManager();
		return (Winchesterb2bCoreManager) em.getExtension(Winchesterb2bCoreConstants.EXTENSIONNAME);
	}
}
