/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.winchester.b2b.fulfilmentprocess.jalo;

import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.extension.ExtensionManager;
import com.winchester.b2b.fulfilmentprocess.constants.Winchesterb2bFulfilmentProcessConstants;

public class Winchesterb2bFulfilmentProcessManager extends GeneratedWinchesterb2bFulfilmentProcessManager
{
	public static final Winchesterb2bFulfilmentProcessManager getInstance()
	{
		ExtensionManager em = JaloSession.getCurrentSession().getExtensionManager();
		return (Winchesterb2bFulfilmentProcessManager) em.getExtension(Winchesterb2bFulfilmentProcessConstants.EXTENSIONNAME);
	}
	
}
