/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at 26 Aug 2021, 16:55:41                       ---
 * ----------------------------------------------------------------
 */
package org.winchesterb2bbackoffice.constants;

/**
 * @deprecated since ages - use constants in Model classes instead
 */
@Deprecated(since = "ages", forRemoval = false)
@SuppressWarnings({"unused","cast"})
public class GeneratedYacceleratorbackofficeConstants
{
	public static final String EXTENSIONNAME = "winchesterb2bbackoffice";
	
	protected GeneratedYacceleratorbackofficeConstants()
	{
		// private constructor
	}
	
	
}
