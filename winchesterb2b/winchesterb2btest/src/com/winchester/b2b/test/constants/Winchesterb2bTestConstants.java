/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.winchester.b2b.test.constants;

/**
 * 
 */
public class Winchesterb2bTestConstants extends GeneratedWinchesterb2bTestConstants
{

	public static final String EXTENSIONNAME = "winchesterb2btest";

}
