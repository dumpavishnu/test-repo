/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.winchester.b2b.facades.constants;

/**
 * Global class for all Winchesterb2bFacades constants.
 */
public class Winchesterb2bFacadesConstants extends GeneratedWinchesterb2bFacadesConstants
{
	public static final String EXTENSIONNAME = "winchesterb2bfacades";

	private Winchesterb2bFacadesConstants()
	{
		//empty
	}
}
