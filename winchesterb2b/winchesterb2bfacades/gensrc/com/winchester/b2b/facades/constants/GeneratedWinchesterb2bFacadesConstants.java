/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at 26 Aug 2021, 16:55:41                       ---
 * ----------------------------------------------------------------
 */
package com.winchester.b2b.facades.constants;

/**
 * @deprecated since ages - use constants in Model classes instead
 */
@Deprecated(since = "ages", forRemoval = false)
@SuppressWarnings({"unused","cast"})
public class GeneratedWinchesterb2bFacadesConstants
{
	public static final String EXTENSIONNAME = "winchesterb2bfacades";
	
	protected GeneratedWinchesterb2bFacadesConstants()
	{
		// private constructor
	}
	
	
}
