/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.winchester.b2b.initialdata.constants;

/**
 * Global class for all YAcceleratorInitialData constants.
 */
public final class Winchesterb2bInitialDataConstants extends GeneratedWinchesterb2bInitialDataConstants
{
	public static final String EXTENSIONNAME = "winchesterb2bb2binitialdata";

	private Winchesterb2bInitialDataConstants()
	{
		//empty
	}
}
